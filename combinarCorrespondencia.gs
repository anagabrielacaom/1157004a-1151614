function menu() {
  var ui = SpreadsheetApp.getUi();
  var topMenu = ui.createMenu('Seleccionar');
  topMenu.addItem('Combinar correspondencia', 'Datos');
  topMenu.addItem('Enviar pdf', 'crearPDF');
  topMenu.addToUi();
}
function Datos(){
  
  //Obtener datos de la hoja de calculo
  var sps = SpreadsheetApp.getActive();
  var rango = sps.getSheets();
  //Tomar todos los datos
  var data = rango[0].getDataRange().getValues();//sacar los datos, todas la informacion de las celdas 
  
  var doc = DocumentApp.openByUrl('https://docs.google.com/document/d/1DScwzzVAZZNphENZb3UvXv6_wLFJwqVHe2riz-Ct338/edit');
  var body = doc.getBody();
  var table = body.appendTable();
  for (var i=0;i<data.length;i++){
    var row=table.appendTableRow();
    for(var j=0;j<7;j++){   
	    var rowD=data[i];
	    var codigo=j;
        var col1row1 = row.appendTableCell(rowD[codigo]);
        codigo+1;
        }
  }  
  var table = doc.getBody().appendTable();
  //doc.setText(table);
  doc.saveAndClose();
}
function  crearPDF ( ) {
   
   const documentoId ='1DScwzzVAZZNphENZb3UvXv6_wLFJwqVHe2riz-Ct338';
   const idCarpetaDrive='1xAc4lwUMUruoD9pJyd0qFb2GaWJnXCt1';
  
   var documento = DriveApp.getFileById(documentoId);
   var carpetaDrive = DriveApp.getFolderById(idCarpetaDrive);
   
 //copia del archivo docs
  const archivoTemporal = documento;
  var documentoPdf = DocumentApp.openById(archivoTemporal.getId());
 // el documento del pdf
  const pdf = archivoTemporal.getAs(MimeType.PDF);
  // Creo el PDF
  //carpetaDrive.createFile(pdf).setName("1151007A_1151614");
  var pdefe=carpetaDrive.createFile(pdf).setName("1151007A_1151614");
  pdefe.setSharing(DriveApp.Access.ANYONE, DriveApp.Permission.VIEW);
  //Correo 
  var asunto;
  var fila=2;
  var direccion;
  
  while(SpreadsheetApp.getActive().getActiveSheet().getRange(fila, 1).getValue() != ""){

   direccion = SpreadsheetApp.getActive().getActiveSheet().getRange(fila, 7).getValue();
         
   GmailApp.sendEmail(direccion,"Envio calificaciones",pdefe.getUrl());
   fila++
 
  }
  //al finalizar el bucle dejaré un mensaje de alerta
  SpreadsheetApp.getUi().alert("Mensajes enviados con éxito");
 
}
